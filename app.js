"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var Article = (function () {
    function Article(points, title, link) {
        this.points = points || 0;
        this.title = title;
        this.link = link;
    }
    Article.prototype.voteUp = function () {
        this.points += 1;
    };
    Article.prototype.voteDown = function () {
        this.points -= 1;
    };
    Article.prototype.domain = function () {
        try {
            var link = this.link.split("//")[1];
            return link.split("/")[0];
        }
        catch (err) {
            return null;
        }
    };
    return Article;
}());
var RedditCreator = (function () {
    function RedditCreator() {
        this.articles = [
            new Article(0, 'angular2', 'http://goodarzif.ir'),
            new Article(5, 'ang2', 'http://goodfargood.com/lessthanmore')
        ];
    }
    RedditCreator.prototype.addArticle = function (title, link) {
        console.log("this is title:  " + title.value + "  and this is link:  " + link.value);
        this.articles.push(new Article(0, title.value, link.value));
        title.value = '';
        link.value = '';
    };
    RedditCreator.prototype.sortedArticles = function () {
        return this.articles.sort(function (a, b) { return b.points - a.points; });
    };
    RedditCreator = __decorate([
        core_1.Component({
            selector: "reddit-creator",
            template: "\n\t<div class=\"col-lg-10  \" style=\"padding-top:1%;    float: none; margin: 0 auto;\">\n\t<form>\n\t\t<div class=\"form-group\" >\n\t\t\t<label for=\"title\" > title: </label>\n\t\t\t<input type=\"text\" class=\"form-control\" #title />\n\t\t</div>\n\t\t<div class=\"form-group\">\n\t\t\t<label for=\"link\"> link:  </label>\n\t\t\t<input type=\"text\" class=\"form-control\" #link />\n\t\t</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t<input value=\"send!\" type=\"button\" class=\"btn btn-primary\" (click)=\"addArticle(title,link)\" />\n\t\t</div>\n\t</form> \n\t<div>\n\t<redit-article *ngFor=\"let article of sortedArticles()\"  [article]=\"article\"></redit-article>\n\t</div>\n\t</div>"
        }), 
        __metadata('design:paramtypes', [])
    ], RedditCreator);
    return RedditCreator;
}());
var RedditArticle = (function () {
    function RedditArticle() {
    }
    RedditArticle.prototype.voteUp = function () {
        this.article.voteUp();
        return false;
    };
    RedditArticle.prototype.voteDown = function () {
        this.article.voteDown();
        return false;
    };
    RedditArticle = __decorate([
        core_1.Component({
            selector: "redit-article",
            host: {
                class: 'row'
            },
            inputs: ['article'],
            template: "\n\t\t<div class=\"row\" style=\"float:none;margin:0 auto; width:80%;\">\n\t\t\t<div class=\"col-lg-4\" style=\"float:left;\">\n\t\t\t\t<h1 style=\"padding:30%;background-color:gray;\">{{ article.points }} </h1>\n\t\t\t</div>\n\t\t\t<div class=\"col-lg-6\" style=\"float:right;\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<h1 >{{ article.title }}</h1>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<a href=\"{{ article.link }}\"> {{ article.domain() }}</a>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<button class=\"btn btn-danger\" (click)=\"voteUp()\">vote up</button>\n\t\t\t\t\t<button class=\"btn btn-success\" (click)=\"voteDown()\"> vote down </button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div> "
        }), 
        __metadata('design:paramtypes', [])
    ], RedditArticle);
    return RedditArticle;
}());
var RedditAppModule = (function () {
    function RedditAppModule() {
    }
    RedditAppModule = __decorate([
        core_1.NgModule({
            declarations: [
                RedditCreator, RedditArticle],
            imports: [platform_browser_1.BrowserModule],
            bootstrap: [RedditCreator]
        }), 
        __metadata('design:paramtypes', [])
    ], RedditAppModule);
    return RedditAppModule;
}());
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(RedditAppModule);
//# sourceMappingURL=app.js.map